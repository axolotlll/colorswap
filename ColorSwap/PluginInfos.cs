﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorSwap
{

    internal static class PluginInfos
    {
        public const string PLUGIN_ID = "com.gitlab.axolotlll.llb-colorswap";
        public const string PLUGIN_NAME = "ColorSwap";
        public const string PLUGIN_VERSION = "1.0.3";
    }
}
