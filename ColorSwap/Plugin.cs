﻿using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using LLBML.GameEvents;
using LLBML.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ColorSwap
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public ConfigEntry<bool> alwaysOutlines;
        public ConfigEntry<bool> noFlicker;


        public ConfigEntry<bool> replaceSprites;
        public ConfigEntry<bool> spriteMultiply;
        public ConfigEntry<int> spriteDarkness;

        public ConfigEntry<string> spriteColoring;

        public ConfigEntry<bool> p1Enabled;
        public ConfigEntry<int> p1R;
        public ConfigEntry<int> p1G;
        public ConfigEntry<int> p1B;

        public ConfigEntry<bool> p2Enabled;
        public ConfigEntry<int> p2R;
        public ConfigEntry<int> p2G;
        public ConfigEntry<int> p2B;

        public ConfigEntry<bool> p3Enabled;
        public ConfigEntry<int> p3R;
        public ConfigEntry<int> p3G;
        public ConfigEntry<int> p3B;

        public ConfigEntry<bool> p4Enabled;
        public ConfigEntry<int> p4R;
        public ConfigEntry<int> p4G;
        public ConfigEntry<int> p4B;

        public static Plugin instance;

        private bool hasChanges;

        public Color p1;
        public Color p2;
        public Color p3;
        public Color p4;

        public Sprites p1Sprites;
        public Sprites p2Sprites;
        public Sprites p3Sprites;
        public Sprites p4Sprites;
        
        void Awake()
        {
            instance = this;
            InitConfig();

            var harmony = new Harmony(PluginInfos.PLUGIN_ID);
            harmony.PatchAll();
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);
            Config.SettingChanged += SettingChanged;
            GameStateEvents.OnStateChange += StateChanged;

            LoadColors();
            LoadSprites();
        }

        /*
         * Event called whenever the user changes a setting from modmenu (i.e. a slider) 
         */
        void SettingChanged(object sender, SettingChangedEventArgs e)
        {
            hasChanges = true;
            // Logger.LogInfo("Has changes to make next game state");
        }

        /*
         *  Load changes & generate new sprites when the state changed (usually when you enter a lobby)
         *  
         *  This is because loading them every time the settings changed would be laggy and I could
         *  not find an event for when you leave the modmenu.
         */
        void StateChanged(object sender, OnStateChangeArgs e)
        {
            if (hasChanges)
            {
                Logger.LogInfo("Making changes");
                LoadColors();
                LoadSprites();
                hasChanges = false;
            }
            
        }

   
        /*
         * load colors from config for use in patch
         */
        void LoadColors()
        {
            p1 = new Color((float)Plugin.instance.p1R.Value / 255f, (float)Plugin.instance.p1G.Value / 255f, (float)Plugin.instance.p1B.Value / 255f);
            p2 = new Color((float)Plugin.instance.p2R.Value / 255f, (float)Plugin.instance.p2G.Value / 255f, (float)Plugin.instance.p2B.Value / 255f);
            p3 = new Color((float)Plugin.instance.p3R.Value / 255f, (float)Plugin.instance.p3G.Value / 255f, (float)Plugin.instance.p3B.Value / 255f);
            p4 = new Color((float)Plugin.instance.p4R.Value / 255f, (float)Plugin.instance.p4G.Value / 255f, (float)Plugin.instance.p4B.Value / 255f);
        }

        /*
         * generates sprites 
         */
        void LoadSprites()
        {
            p1Sprites = new Sprites();
            p2Sprites = new Sprites();
            p3Sprites = new Sprites();
            p4Sprites = new Sprites();

            p1Sprites.setColors(p1);
            p2Sprites.setColors(p2);
            p3Sprites.setColors(p3);
            p4Sprites.setColors(p4);
        }

        /*
         * called during the game to check if there is a sprite for replacement
         * 
         * returns null, if it should not replace this sprite.
         */
        public static Texture2D GetSpriteReplacement(string spriteName)
        {
            if (spriteName.StartsWith("aura_"))
            {
                string last = spriteName[spriteName.Length - 1].ToString();
                if (last == "0") return Plugin.instance.p1Sprites.aura;
                else if (last == "1") return Plugin.instance.p2Sprites.aura;
                else if (last == "2") return Plugin.instance.p3Sprites.aura;
                else if (last == "3") return Plugin.instance.p4Sprites.aura;
                return null;
            }

            string lastChar = spriteName[spriteName.Length - 1].ToString();

            if (lastChar == "1" && !Plugin.instance.p1Enabled.Value) return null;
            if (lastChar == "2" && !Plugin.instance.p2Enabled.Value) return null;
            if (lastChar == "3" && !Plugin.instance.p3Enabled.Value) return null;
            if (lastChar == "4" && !Plugin.instance.p4Enabled.Value) return null;

            if (lastChar == "0") return null;
            if (spriteName.StartsWith("serveGlowEffect_")) return GetSprites(lastChar).serveGlowEffect;
            else if (spriteName.StartsWith("smashBlast_")) return GetSprites(lastChar).smashBlast;
            else if (spriteName.StartsWith("smashBlastSmall_")) return GetSprites(lastChar).smashBlastSmall;
            else if (spriteName.StartsWith("deflectHitEffect_")) return GetSprites(lastChar).deflectHitEffect;
            else if (spriteName.StartsWith("bite_")) return GetSprites(lastChar).bite;
            else if (spriteName.StartsWith("ballAppear_")) return GetSprites(lastChar).ballAppear;
            else if (spriteName.StartsWith("getDrawIn_")) return GetSprites(lastChar).getDrawIn;
            else if (spriteName.StartsWith("hitEffect_")) return GetSprites(lastChar).hitEffect;

            return null;
        }

        private static Sprites GetSprites(string index)
        {
            if (index == "1") return Plugin.instance.p1Sprites;
            else if (index == "2") return Plugin.instance.p2Sprites;
            else if (index == "3") return Plugin.instance.p3Sprites;
            else if (index == "4") return Plugin.instance.p4Sprites;
            return null;
        }

        /*
         * setup ModMenu config
         */
        void InitConfig()
        {
            Config.Bind(
                "Outlines",
                "mm_header_Outlines",
                "Outlines",
                new ConfigDescription("", null, "modmenu_header")
            );

            alwaysOutlines = Config.Bind<bool>("Toggles", "alwaysOutlines", false, "Outlines are on all the time.");
            noFlicker = Config.Bind<bool>("Toggles", "noFlicker", false, "Stops outline flickering effect during hitlag.");

            Config.Bind("gap",
                "mm_header_gap",
                20,
                new ConfigDescription("", null, "modmenu_gap")
                );

            Config.Bind("Sprite Settings",
                "mm_header_sprites",
                "Sprite Settings",
                new ConfigDescription("", null, "modmenu_header"));
            replaceSprites = Config.Bind<bool>("Toggles", "Recolor Sprites", true, "Replace sprites in-game for recoloring");
            spriteMultiply = Config.Bind<bool>("Toggles", "Multiply (Enabled), Screen (Disabled)", false, "True for Multiply, False for Screen.");
            spriteColoring = Config.Bind<string>("Toggles", 
                "Sprite Coloring", 
                "Screen", 
                new ConfigDescription("Screen", new AcceptableValueList<string>("Screen", "Multiply")));
            spriteDarkness = Config.Bind<int>("Tuning", 
                "spriteDarkness", 10, new ConfigDescription("Darkness of sprites", new AcceptableValueRange<int>(1, 10)));
            
            Config.Bind("gap",
                "mm_header_gap",
                20,
                new ConfigDescription("", null, "modmenu_gap")
                ); ;

            Config.Bind(
                "P1Color",
                "mm_header_P1",
                "P1 Color",
                new ConfigDescription("", null, "modmenu_header")
                );
            p1Enabled = Config.Bind<bool>("Toggles", "p1Enabled", false, "Enables custom P1 color");
            p1R = Config.Bind<int>("Tuning",
                "p1R", 255, new ConfigDescription("P1 Red Value", new AcceptableValueRange<int>(0, 255)));
            p1G = Config.Bind<int>("Tuning",
                "p1G", 63, new ConfigDescription("P1 Green Value", new AcceptableValueRange<int>(0, 255)));
            p1B = Config.Bind<int>("Tuning",
                "p1B", 25, new ConfigDescription("P1 Blue Value", new AcceptableValueRange<int>(0, 255)));
            Config.Bind(
                "P2Color",
                "mm_header_P2",
                "P2 Color",
                new ConfigDescription("", null, "modmenu_header")
                );
            p2Enabled = Config.Bind<bool>("Toggles", "p2Enabled", false, "Enables custom p2 color");
            p2R = Config.Bind<int>("Tuning",
                "p2R", 12, new ConfigDescription("P2 Red Value", new AcceptableValueRange<int>(0, 255)));
            p2G = Config.Bind<int>("Tuning",
                "p2G", 136, new ConfigDescription("P2 Green Value", new AcceptableValueRange<int>(0, 255)));
            p2B = Config.Bind<int>("Tuning",
                "p2B", 255, new ConfigDescription("P2 Blue Value", new AcceptableValueRange<int>(0, 255)));
            Config.Bind(
                "P3Color",
                "mm_header_P3",
                "P3 Color",
                new ConfigDescription("", null, "modmenu_header")
                );
            p3Enabled = Config.Bind<bool>("Toggles", "p3Enabled", false, "Enables custom p3 color");
            p3R = Config.Bind<int>("Tuning",
                "p3R", 255, new ConfigDescription("P3 Red Value", new AcceptableValueRange<int>(0, 255)));
            p3G = Config.Bind<int>("Tuning",
                "p3G", 255, new ConfigDescription("P3 Green Value", new AcceptableValueRange<int>(0, 255)));
            p3B = Config.Bind<int>("Tuning",
                "p3B", 61, new ConfigDescription("P3 Blue Value", new AcceptableValueRange<int>(0, 255)));
            Config.Bind(
                "P4Color",
                "mm_header_P4",
                "P4 Color",
                new ConfigDescription("", null, "modmenu_header")
                );
            p4Enabled = Config.Bind<bool>("Toggles", "p4Enabled", false, "Enables custom p4 color");
            p4R = Config.Bind<int>("Tuning",
                "p4R", 90, new ConfigDescription("P4 Red Value", new AcceptableValueRange<int>(0, 255)));
            p4G = Config.Bind<int>("Tuning",
                "p4G", 244, new ConfigDescription("P4 Green Value", new AcceptableValueRange<int>(0, 255)));
            p4B = Config.Bind<int>("Tuning",
                "p4B", 90, new ConfigDescription("P4 Blue Value", new AcceptableValueRange<int>(0, 255)));

        }

        
    }
}
