﻿using GameplayEntities;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ColorSwap
{

    /*
     * Outlines
     */
    [HarmonyPatch(typeof(PlayerEntity), nameof(PlayerEntity.UpdateState))]
    public class PlayerPatch
    {
        public static void Postfix(PlayerEntity __instance)
        {
            if (Plugin.instance.alwaysOutlines.Value)
            {
                __instance.SetColorOutlines(true);
                if (!__instance.GetCurrentAbilityState().blinkColorOutline)
                {
                    __instance.SetColorOutlinesColor(__instance.playerData.team);
                }
            }
            if (Plugin.instance.noFlicker.Value)
            {
                __instance.SetColorOutlinesColor(__instance.playerData.team);

            }
        }
    }

    [HarmonyPatch(typeof(JPLELOFJOOH), nameof(JPLELOFJOOH.FJOIEBBLMMI))]
    public class ColorPatch
    {
        public static bool Prefix(BGHNEHPFHGC HEOKEMBMDIJ, ref Color __result)
        {
            if (HEOKEMBMDIJ == BGHNEHPFHGC.EHPJJADIPNG && Plugin.instance.p1Enabled.Value)
            {
                __result = Plugin.instance.p1;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.PDOIGCCJJEO && Plugin.instance.p2Enabled.Value)
            {
                __result = Plugin.instance.p2;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.JAGOLMCFMHD && Plugin.instance.p3Enabled.Value)
            {
                __result = Plugin.instance.p3;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.LHCCOPMFNOC && Plugin.instance.p4Enabled.Value)
            {
                __result = Plugin.instance.p4;
                return false;
            }
            return true;
        }
    }
    //

    [HarmonyPatch(typeof(JPLELOFJOOH), nameof(JPLELOFJOOH.FCAAOAPLCMI))]
    public class Color2Patch
    {
        public static bool Prefix(BGHNEHPFHGC HEOKEMBMDIJ, ref Color __result)
        {
            if (HEOKEMBMDIJ == BGHNEHPFHGC.EHPJJADIPNG && Plugin.instance.p1Enabled.Value)
            {
                __result = Plugin.instance.p1;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.PDOIGCCJJEO && Plugin.instance.p2Enabled.Value)
            {
                __result = Plugin.instance.p2;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.JAGOLMCFMHD && Plugin.instance.p3Enabled.Value)
            {
                __result = Plugin.instance.p3;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.LHCCOPMFNOC && Plugin.instance.p4Enabled.Value)
            {
                __result = Plugin.instance.p4;
                return false;
            }
            return true;
        }
    }

    /*
     * ball trail
     */
    [HarmonyPatch(typeof(JPLELOFJOOH), nameof(JPLELOFJOOH.EGJAKCLDCGB))]
    public class Color3Patch
    {
        public static bool Prefix(BGHNEHPFHGC HEOKEMBMDIJ, ref Color __result)
        {
                if (HEOKEMBMDIJ == BGHNEHPFHGC.EHPJJADIPNG && Plugin.instance.p1Enabled.Value)
            {
                __result = Plugin.instance.p1;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.PDOIGCCJJEO && Plugin.instance.p2Enabled.Value)
            {
                __result =
                    Plugin.instance.p2;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.JAGOLMCFMHD && Plugin.instance.p3Enabled.Value)
            {
                __result = Plugin.instance.p3;
                return false;
            }
            else if (HEOKEMBMDIJ == BGHNEHPFHGC.LHCCOPMFNOC && Plugin.instance.p4Enabled.Value)
            {
                __result = Plugin.instance.p4;
                return false;
            }
            return true;
        }
    }





    /*
     *  Patch for non-team color sprites
     */
    [HarmonyPatch(typeof(VisualEntity), nameof(VisualEntity.SetVisualSprite))]
    [HarmonyPatch(new Type[] {
        typeof(string),
        typeof(string),
        typeof(bool),
        typeof(bool),
        typeof(JKMAAHELEMF),
        typeof(JKMAAHELEMF),
        typeof(float),
        typeof(bool),
        typeof(float),
        typeof(Layer),
        typeof(Color32)
    })]
    public class Sprite2Patch
    {
        public static bool Prefix(VisualEntity __instance, ref Texture2D __result,
            string visualName,
            string spriteName,
            bool withShadow,
            bool withOutline,
            JKMAAHELEMF frameSizePixels,
            JKMAAHELEMF offsetPixels,
            float ZPosOffset,
            bool shadowMovesY,
            float shadowZOffset,
            Layer pLayer,
            Color32 tint)
        {
            if (!Plugin.instance.replaceSprites.Value) return true;
            Texture2D replacement = Plugin.GetSpriteReplacement(spriteName);
            if (replacement != null)
            {
                __instance.SetVisualSprite(visualName, replacement, withShadow, withOutline, frameSizePixels, offsetPixels, ZPosOffset, shadowMovesY, shadowZOffset, pLayer, tint);
                __result = replacement;
                return false;
            }
            return true;
        }
    }

    /*
     *  fix serve glow effect
     */
    [HarmonyPatch(typeof(BouncableEntity), nameof(BouncableEntity.SetToTeam))]
    public class BallPatch
    {
        public static void Postfix(BouncableEntity __instance, BGHNEHPFHGC team)
        {
            if (!Plugin.instance.replaceSprites.Value) return;
            if (team == BGHNEHPFHGC.EHPJJADIPNG && Plugin.instance.p1Enabled.Value)
            {
                __instance.GetVisual("glowVisual").mainRenderer.material.mainTexture = Plugin.instance.p1Sprites.serveGlowEffect;
            }
            else if (team == BGHNEHPFHGC.PDOIGCCJJEO && Plugin.instance.p2Enabled.Value)
            {
                __instance.GetVisual("glowVisual").mainRenderer.material.mainTexture = Plugin.instance.p2Sprites.serveGlowEffect;
            }
            else if (team == BGHNEHPFHGC.JAGOLMCFMHD && Plugin.instance.p3Enabled.Value)
            {
                __instance.GetVisual("glowVisual").mainRenderer.material.mainTexture = Plugin.instance.p3Sprites.serveGlowEffect;
            }
            else if(team== BGHNEHPFHGC.LHCCOPMFNOC && Plugin.instance.p4Enabled.Value)
            {
                __instance.GetVisual("glowVisual").mainRenderer.material.mainTexture = Plugin.instance.p4Sprites.serveGlowEffect;
            }
        }
    }
}
