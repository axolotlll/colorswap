﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ColorSwap
{
    public class Sprites
    {
        public Texture2D serveGlowEffect;
        public Texture2D smashBlast;
        public Texture2D smashBlastSmall;
        public Texture2D deflectHitEffect;
        public Texture2D bite;
        public Texture2D ballAppear;
        public Texture2D getDrawIn;
        public Texture2D hitEffect;
        public Texture2D serveIndicator;
        public Texture2D aura;

        public Sprites()
        {
            setCopy(ref serveGlowEffect, JPLELOFJOOH.DJILOODDJDC("serveGlowEffect", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref smashBlast, JPLELOFJOOH.DJILOODDJDC("smashBlast", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref smashBlastSmall, JPLELOFJOOH.DJILOODDJDC("smashBlastSmall", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref deflectHitEffect, JPLELOFJOOH.DJILOODDJDC("deflectHitEffect", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref bite, JPLELOFJOOH.DJILOODDJDC("bite", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref ballAppear, JPLELOFJOOH.DJILOODDJDC("ballAppear", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref getDrawIn, JPLELOFJOOH.DJILOODDJDC("getDrawIn", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref hitEffect, JPLELOFJOOH.DJILOODDJDC("hitEffect", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref serveIndicator, JPLELOFJOOH.DJILOODDJDC("serveIndicator", BGHNEHPFHGC.EHPJJADIPNG, false));
            setCopy(ref aura, JPLELOFJOOH.DJILOODDJDC("aura_0", false));
        }

        /*
         * copy from game asset to a new Texture2D.
         */
        private void setCopy(ref Texture2D dst, Texture2D source)
        {
            RenderTexture tmp = RenderTexture.GetTemporary(
                source.width,
                source.height,
                0,
                RenderTextureFormat.Default,
                RenderTextureReadWrite.Linear);

            Graphics.Blit(source, tmp);

            RenderTexture prev = RenderTexture.active;
            RenderTexture.active = tmp;
            dst = new Texture2D(source.width, source.height);
            dst.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
            dst.Apply();
            RenderTexture.active = prev;
            RenderTexture.ReleaseTemporary(tmp);
        }

        /*
         * set color of texture to team color, based on some parameters from config
         */
        private void setColor(ref Texture2D tex, Color color)
        {
            Color[] pix = tex.GetPixels();
            for (int i = 0; i < pix.Length; i++)
            {
                //weighting from wikipedia
                //float avg = 0.299f * pix[i].r + 0.587f * pix[i].g + 0.114f * pix[i].b;

                // tweak for a red image (p1)
                float darkness = Plugin.instance.spriteDarkness.Value / 10;
                /*if (Plugin.instance.spriteMultiply.Value)
                {
                    darkness = Plugin.instance.spriteDarkness.Value / 10;
                }
                else 
                {
                    darkness = 1f - (Plugin.instance.spriteDarkness.Value / 10);
                }*/
                float avg = (1-darkness) * pix[i].r + (darkness / 2f) * pix[i].g + (darkness / 2f)* pix[i].b;

                if (Plugin.instance.spriteMultiply.Value)
                {
                    pix[i] = new Color(
                    color.r * avg,
                    color.g * avg,
                    color.b * avg,
                    pix[i].a);
                }

                else
                {
                    pix[i] = new Color(
                    1f - (1f - color.r) * (1f - avg),
                    1f - (1f - color.g) * (1f - avg),
                    1f - (1f - color.b) * (1f - avg),
                    pix[i].a);
                }
                
                /* grayscale testing*/
                //pix[i] = new Color(avg, avg, avg, pix[i].a);
            }
            tex.SetPixels(pix);
            tex.Apply();
        }

        /*
         * set all sprites to team color
         */
        public void setColors(Color color)
        {
            setColor(ref serveGlowEffect, color);
            setColor(ref smashBlast, color);
            setColor(ref smashBlastSmall, color);
            setColor(ref deflectHitEffect, color);
            setColor(ref bite, color);
            setColor(ref ballAppear, color);
            setColor(ref getDrawIn, color);
            setColor(ref hitEffect, color);
        }
    }
}
